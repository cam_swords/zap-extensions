/*
 * Zed Attack Proxy (ZAP) and its related class files.
 *
 * ZAP is an HTTP/HTTPS proxy for assessing web application security.
 *
 * Copyright 2022 The ZAP Development Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zaproxy.addon.network.internal.client;

import java.io.IOException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.Objects;

import io.netty.util.NetUtil;
import org.apache.commons.lang.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zaproxy.addon.network.internal.cert.CertData;
import org.zaproxy.addon.network.internal.cert.CertificateUtils;
import org.zaproxy.addon.network.internal.cert.ServerCertificateService;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocketFactory;

/** A certificate of a {@link KeyStoreEntry}. */
public class CertificateEntry {

    private static final Logger LOGGER = LogManager.getLogger(CertificateEntry.class);

    private final KeyStoreEntry parent;
    private final Certificate certificate;
    private final String alias;
    private final String name;
    private final int index;
    private SSLContext sslContext;

    CertificateEntry(KeyStoreEntry parent, Certificate certificate, String alias, int index) {
        this.parent = Objects.requireNonNull(parent);
        this.certificate = Objects.requireNonNull(certificate);
        this.alias = Objects.requireNonNull(alias);
        this.name = extractName(certificate, alias);
        this.index = index;
    }

    public KeyStoreEntry getParent() {
        return parent;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public boolean isUnlocked() {
        return sslContext != null;
    }

    public boolean unlock(String password) {
        throw new NotImplementedException("unlock certificate entry");
    }

    private static void initKeyManagerFactoryWithCertForHostname(
            ServerCertificateService certificateService,
            KeyManagerFactory keyManagerFactory,
            String hostname,
            InetAddress listeningAddress)
            throws GeneralSecurityException, IOException {

        boolean hostnameIsIpAddress = isIpAddress(hostname);

        CertData certData = hostnameIsIpAddress ? new CertData() : new CertData(hostname);
        if (hostname == null) {
            certData.addSubjectAlternativeName(
                    new CertData.Name(CertData.Name.IP_ADDRESS, listeningAddress.getHostAddress()));
        }

        if (hostnameIsIpAddress) {
            certData.addSubjectAlternativeName(
                    new CertData.Name(CertData.Name.IP_ADDRESS, hostname));
        }

        KeyStore ks = certificateService.createCertificate(certData);
        keyManagerFactory.init(ks, CertificateUtils.getPassphrase());
    }

    private static boolean isIpAddress(String value) {
        return value != null
                && !value.isEmpty()
                && (NetUtil.isValidIpV4Address(value) || NetUtil.isValidIpV6Address(value));
    }

    public SSLContext getSslContext() {
        return sslContext;
    }

    public SSLSocketFactory getSocketFactory() {
        if (sslContext == null) {
            return null;
        }
        return sslContext.getSocketFactory();
    }

    void invalidateSession() {
        if (sslContext == null) {
            return;
        }

        invalidateSession(sslContext.getClientSessionContext());
        invalidateSession(sslContext.getServerSessionContext());
    }

    private static void invalidateSession(SSLSessionContext session) {
        if (session == null) {
            return;
        }

        int timeout = session.getSessionTimeout();
        session.setSessionTimeout(1);
        session.setSessionTimeout(timeout);
    }

    private static String extractName(Certificate certificate, String alias) {
        String cn = getCn(certificate);
        if (cn == null || cn.isEmpty()) {
            return alias;
        }
        return cn + " [" + alias + "]";
    }

    private static String getCn(Certificate certificate) {
        String dn = certificate.toString();
        int i = 0;
        i = dn.indexOf("CN=");
        if (i == -1) {
            return null;
        }
        dn = dn.substring(i + 3);

        char[] dnChars = dn.toCharArray();
        for (i = 0; i < dnChars.length; i++) {
            if (dnChars[i] == ',' && i > 0 && dnChars[i - 1] != '\\') {
                break;
            }
        }
        return dn.substring(0, i);
    }

    @Override
    public String toString() {
        return name;
    }
}
